package com.brane.demo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class UserService {
	@Autowired
	UserRepository userRepository;
	public void save(UserInfo user) {
		userRepository.save(user);
		System.out.println(user);
	}
}