package com.brane.demo;

import javax.persistence.*;

@Entity

public class AdminInfo {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Integer id;
	String flightnumber,airline;
	int businessseats,nonbusinessseats;
	public String getFlightnumber() {
		return flightnumber;
	}
	public void setFlightnumber(String flightnumber) {
		this.flightnumber = flightnumber;
	}
	public String getAirline() {
		return airline;
	}
	public void setAirline(String airline) {
		this.airline = airline;
	}
	public int getBusinessseats() {
		return businessseats;
	}
	public void setBusinessseats(int businessseats) {
		this.businessseats = businessseats;
	}
	public int getNonbusinessseats() {
		return nonbusinessseats;
	}
	public void setNonbusinessseats(int nonbusinessseats) {
		this.nonbusinessseats = nonbusinessseats;
	}

}
