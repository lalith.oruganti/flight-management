package com.brane.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
@RestController
public class FlightController {
	@Autowired
	UserService userService;
	@GetMapping("/")
	public void fetchresults() {
		System.out.println("Userdetails");
	}
	@PostMapping("/user")
	public void savedetails(@RequestBody UserInfo userinfo)
	{
		userService.save(userinfo);
		System.out.println("name of the user--"+userinfo.getName());
		System.out.println("email of the user--"+userinfo.getEmail());
		System.out.println("number of seats user booked--"+userinfo.getNumberofseats());
		System.out.println("and also opted for Meal type as--"+userinfo.getMeal());
	}
	@PostMapping("/admin")
	public void savedetails(@RequestBody AdminInfo admininfo)
	{
		System.out.println("Airline name--"+admininfo.getAirline());
		System.out.println("Flight number--"+admininfo.getFlightnumber()+" having "+admininfo.getBusinessseats()+" business seats and "+admininfo.getNonbusinessseats()+" non-business seats");
	}
	@PostMapping("/search")
	public String savedetails(@RequestBody Search search) {
		System.out.println("flightid:"+search.getFlightid());
		System.out.println("route:"+search.getRoute());
		System.out.println("timings:"+search.getTimings());
		return "post called";
	}
	@PostMapping("/Booking")
	public String savedetails(@RequestBody Booking booking) {
		System.out.println("flightid:"+booking.getFlightid());
		return "post called";
	}

}
