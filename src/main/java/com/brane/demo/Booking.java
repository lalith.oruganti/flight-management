package com.brane.demo;

public class Booking {

	public Booking(String flightid, UserInfo user, AdminInfo admins) {
		super();
		this.flightid = flightid;
		this.user = user;
		this.admins = admins;
	}
	private String flightid;
	public String getFlightid() {
		return flightid;
	}
	public void setFlightid(String flightid) {
		this.flightid = flightid;
	}
	public UserInfo getUser() {
		return user;
	}
	public void setUser(UserInfo user) {
		this.user = user;
	}
	public AdminInfo getAdmins() {
		return admins;
	}
	public void setAdmins(AdminInfo admins) {
		this.admins = admins;
	}
	private UserInfo user;
	private AdminInfo admins;
	

}
