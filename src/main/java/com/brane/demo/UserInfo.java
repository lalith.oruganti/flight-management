package com.brane.demo;

import javax.persistence.*;

@Entity
public class UserInfo {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Integer id;
	String email,name;
	int numberofseats;
	String meal;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNumberofseats() {
		return numberofseats;
	}
	public void setNumberofseats(int numberofseats) {
		this.numberofseats = numberofseats;
	}
	public String getMeal() {
		return meal;
	}
	public void setMeal(String meal) {
		this.meal = meal;
	}
	
	
}
